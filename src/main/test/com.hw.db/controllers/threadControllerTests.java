package com.hw.db.controllers;

import java.util.*;
import com.hw.db.DAO.*;
import com.hw.db.models.*;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.*;
import static org.junit.jupiter.api.*;
import static org.mockito.*;

public class threadControllerTests {

	private threadController threadController = new threadController();

	@BeforeEach
	void setUp()
	{
		User user = new User("Bob", "bob@bob.bob", "Bob Bob", "I'm Bob");
    Thread thread = new Thread("test", new Timestamp(System.currentTimeMillis()), "test", "test", "test", "test", 0);
  }

  @Test
  @DisplayName("Testing post creation...")
	void createPostTest() {
    try(MockedStatic<ThreadDAO> mockThreadDAO = mockStatic(ThreadDAO.class)) {
      mockThreadDAO
            .when(() -> ThreadDAO.createPosts(any(), any(), any()))
            .thenAnswer((Answer<Void>) invocation -> null);
      try (MockedStatic<UserDAO> mockUserDAO = Mockito.mockStatic(UserDAO.class)) {
        mockUserDAO.when(() -> UserDAO.Info("Bob")).thenReturn(user);
        String slug = "test";
        Post[] posts = new ArrayList<Post>();
        threadController.createPost(slug, posts);
        mockThreadDAO.verify(() -> ThreadDAO.createPosts(thread, posts, List.of(user)));
      }
    }
  }

  @Test
  @DisplayName("Testing post changing...")
	void changePostTest() {
    try(MockedStatic<ThreadDAO> mockThreadDAO = mockStatic(ThreadDAO.class)) {
      mockThreadDAO.when(ThreadDAO.getThreadById(Mockito.any())).thenReturn(thread);
      mockThreadDAO.when(ThreadDAO.getThreadBySlug(Mockito.any())).thenReturn(thread);
      String id = "1";
      thread.setId(Integer.parseInt(id));
      threadController.change(id, thread);
      mockThreadDAO.verify(() -> ThreadDAO.change(thread, thread));
    }
  }

	@Test
  @DisplayName("Testing getting thread by slug...")
	void checkSlugTest() {
    try(MockedStatic<ThreadDAO> mockThreadDAO = mockStatic(ThreadDAO.class)) {
      mockThreadDAO.when(() -> ThreadDAO.getThreadBySlug(thread.getSlug())).thenReturn(thread);
      String slug = "test";
      threadController.CheckIdOrSlug(slug);
      mockThreadDAO.verify(() -> ThreadDAO.getThreadBySlug(slug));
    }
  }

  @Test
  @DisplayName("Testing getting thread by id...")
	void checkSlugTest() {
    try(MockedStatic<ThreadDAO> mockThreadDAO = mockStatic(ThreadDAO.class)) {
      mockThreadDAO.when(() -> ThreadDAO.getThreadBySlug(thread.getSlug())).thenReturn(thread);
      String id = "0";
      threadController.CheckIdOrSlug(id);
      mockThreadDAO.verify(() -> ThreadDAO.getThreadBySlug(id));
    }
  }

	@Test
  @DisplayName("Testing getting posts...")
	void getPostsTest() {
    try(MockedStatic<ThreadDAO> mockThreadDAO = mockStatic(ThreadDAO.class)) {
      mockThreadDAO
            .when(() -> ThreadDAO.getPosts(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
            .thenAnswer((Answer<List<Post>>) invocation -> new ArrayList<Post>());
      String slug = "test";
      Strin sort = "";
      threadController.Posts(slug, 0, 0, sort, false);
      mockThreadDAO.verify(() -> ThreadDAO.getPosts(slug, 0, 0, sort, false));
    }		
  }

	@Test
	void getPostInfoTest() {
    try(MockedStatic<ThreadDAO> mockThreadDAO = mockStatic(ThreadDAO.class)) {
      mockThreadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any())).thenReturn(thread);
      String slug = "test";
      threadController.info(slug);
      mockThreadDAO.verify(() -> ThreadDAO.getThreadBySlug(slug));
    }
  }


	@Test
	void createVoteTest() {
    try(MockedStatic<ThreadDAO> mockThreadDAO = mockStatic(ThreadDAO.class)) {
      mockThreadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any())).thenReturn(thread);
      try (MockedStatic<UserDAO> mockUserDAO = Mockito.mockStatic(UserDAO.class)) {
        mockUserDAO.when(() -> UserDAO.Info("Bob")).thenReturn(user);
        String slug = "test";
        Vote vote = new Vote("Bob", 0);
        controller.createVote(slug, vote);
        mockThreadDAO.verify(() -> ThreadDAO.createVote(thread, vote));
      }
    }
  }
}